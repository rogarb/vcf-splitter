vcf-splitter
====

Description:
------------
A small script for splitting multi-entry vcf files into single entry files.

The output file are named with the pattern "${N}.vcf", where N is an integer
starting from 1. It is automatically calculated using the number of files in the
output directory.

It can add a `UID` field (needed for [khard](https://github.com/lucc/khard)) if
the `--uid` flag is set.

The input files can be filtered with the `--filter` flag. For example, only the
entries containing an `EMAIL` field can be extracted using:
```
$ vcf-splitter.py --filter EMAIL -o OUTDIR input.vcf
```

Installation:
-------------
Copy the python script somewhere in the `PATH` or use installation script `install.py`.
See `./install.py --help`.

Usage:
------
```
vcf-splitter.py [-h] -o OUTDIR [-f STRING] [-u] input_files [input_files ...]

positional arguments:
  input_files           list of input files in VCARD format

options:
  -h, --help            show this help message and exit
  -o OUTDIR, --outdir OUTDIR
                        output directory to extract the files
  -f STRING, --filter STRING
                        filter the entries containing STRING
  -u, --uid             add a UID field to the entry if missing
```

TODO:
-----
 - Prefix output files with vcf file name
 - Add a `--prefix` flag: prefix for output file name
